# Synechocystis ETC 2022

## Name

Mathematical model of the Photosynthetic Electron Transport Chain (PETC) of *Synechocystis* PCC 6803

## Description

A mathematical, Ordinary Differential Equation (ODE)-based model of the *Synechocystis* PETC.
It captures the major enzymes, complexes, and metabolites that define *Synechocystis* photosynthesis, including linear, cyclic and water-water electron flows, respiration and lumped sink reactions such as the Calvin Benson Bassham (CBB) cycle and Photorespiration (PR).
The model uses a full-spectrum (visible) light description (400 nm - 700 nm) to capture the effect of different light colours on the PETC.
It considers important adaptive processes like state transitions and Non-Photochemical Quenching (NPQ) via the Orange Carotenoid Protein (OCP).

The model is build using the python modelbase package (Van Aalst, [doi: 10.1186/s12859-021-04122-7](https://doi.org/10.1186/s12859-021-04122-7)).

![An image showing the reactions included in the model](model_scheme.svg "Overview over the model")
<sub>Abbreviations: 2PG: 2-phosphoglycolate, 3PGA: 3-phosphoglycerate, ADP:
Adenosine diphosphate, ATP: Adenosine triphosphate, ATPsynth: ATP synthase, CBB: Calvin-Benson-Bassham cycle, CCM: Carbon Concentrating Mechanism, COX(aa3):
Cytochrome c oxidase, Cyd(bd): Cytochrome bd quinol oxidase, Cyto b6 f: Cytochrome b6 f complex, FNR: Ferredoxin-NADP+ Reductase, FQ: Ferredoxin quinol reductase, Fd:
Ferredoxin, Flv: Flavodiiron protein dimer 1/3, NADP+: Nicotinamide adenine dinucleotide phosphate, NADPH: reduced Nicotinamide adenine dinucleotide phosphate, NAD+:
Nicotinamide adenine dinucleotide, NADH: reduced Nicotinamide adenine dinucleotide, NDH-1: NADPH dehydrogenase 1, NDH-2: NADPH dehydrogenase 2, Oxy: RuBisCO
oxygenation, PC: Plastocyanine, PQ: Plastoquinone, PR: Photorespiration, PSI: Photosystem I, PSII: Photosystem II, SDH: Succinate dehydrogenase

## Install

To install in Linux, run the following code within the target folder.
We recommend to use the provided environment (```synechocystis-etc-2022```)

```bash
git clone git@gitlab.com:TPfennig/synechocystis-etc-2022.git
cd synechocystis-etc-2022
chmod u+x INSTALL.sh
./INSTALL.sh

conda env create -f environment.yml
conda activate synechocystis-etc-2022
```

## Usage

The file ```paper_figures.ipynb``` reproduces all publication figures but figure 13.
Figure 13 is produced in the file ```SPInt_fluorescence_experiment_Figure13.xlsx``` in the folder ```data```.

The model is defined within the ```module_...py``` or ```module_update_...py``` python files. A complete model (without OCP) can be created through the ```get_model()``` function within ```get_current_model.py```. To add OCP functionality, use the add_OCP function in ```module_update_phycobilisomes.py```.

All parameters are imported from ```parameters.py``` and calulated within ```calculate_parameters_restruct.py``` . By running the latter from shell, the parameters can be exported to ```parameters.py```.

## Support

For issues contact <tobias.pfennig@rwth-aachen.de>

## Authors

- [Tobias Pfennig](https://www.cpbl.rwth-aachen.de/cms/CPBL/Die-Juniorprofessur/Unser/~wljpm/Tobias-Pfennig/) ([@TPfennig](https://gitlab.com/TPfennig))
- [Elena Kullmann](https://www.cpbl.rwth-aachen.de/cms/CPBL/Die-Juniorprofessur/Unser/~wljyq/Elena-Kullmann/) ([@elena_kullmann](https://gitlab.com/elena_kullmann))
- [Oliver Ebenhöh](https://www.qtb.hhu.de/qtb-team/qtb-team-details?tt_address%5Bfunktion%5D=18463&tt_address%5Bperson%5D=15524&cHash=17d4e241a639119e32e18dab9de14dc5) ([@ebenhoeh](https://gitlab.com/ebenhoeh))
- [Anna Matuszynska](https://www.cpbl.rwth-aachen.de/cms/CPBL/Die-Juniorprofessur/Unser/~sbfpr/Anna-Matuszy-324-ska/) ([@matuszynska](https://gitlab.com/matuszynska))

## Acknowledgements

- [Andreas Nakielski](https://www.cpbl.rwth-aachen.de/cms/CPBL/Die-Juniorprofessur/Unser/~ylubj/Andreas-Nakielski/) ([@AndreasNakielski](https://gitlab.com/AndreasNakielski))
- [Tomáš Zavřel](https://www.czechglobe.cz/en/contacts/144/)
- [Jan Červený](https://www.czechglobe.cz/en/contacts/145/)
- [Gábor Bernát](https://www.blki.hu/en/Gabor.BERNAT)

## How to cite
You can cite this work by referencing the latest publication:

**Shedding Light On Blue-Green Photosynthesis: A Wavelength-Dependent Mathematical Model Of Photosynthesis In *Synechocystis* sp. PCC 6803**<br>
Tobias Pfennig, Elena Kullmann, Tomáš Zavřel, Andreas Nakielski, Oliver Ebenhöh, Jan Červený, Gábor Bernát, Anna Matuszyńska
bioRxiv 2023.06.30.547186; doi: https://doi.org/10.1101/2023.06.30.547186

An archived version of this project is also deposited on Zenodo with the DOI https://doi.org/10.5281/zenodo.8186124

## License

[![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

## Project status

IN PUBLISHING
