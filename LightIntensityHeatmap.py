# Import packages and functions
import modelbase

import numpy as np
import pandas as pd

# import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.colors import TwoSlopeNorm
import os
import importlib
import sys
import re


from modelbase.ode import Model, Simulator, mca
from modelbase.ode import ratelaws as rl
from modelbase.ode import ratefunctions as rf

# Import model
from get_current_model import get_model

from functions import *
import calculate_parameters_restruct as params
import submodules.lightdescription.light_with_pigments as lip

m, y0 = get_model()


def _calculate_coefficients(model, parameter, compound, reaction, intens, wl):
    ccc = []
    fcc = []

    for j in intens:
        model.update_parameter("pfd", lip.light_gaussianLED(wl, j))  #
        _ccc, _fcc = mca.get_response_coefficients(
            model, str(parameter), y=y0, displacement=0.0001, tolerance=1e-3
        )
        ccc.append(
            np.nan if _ccc is None else _ccc[model.compounds.index(compound)]
        )  # control of PS2 over ATP concentration
        fcc.append(
            np.nan if _fcc is None else _fcc[model.get_rate_names().index(reaction)]
        )  # control of PS2 over ATP synthase activity

    return ccc, fcc


def plot_coefficients_heatmap(intensDF, intensFDF, intens):

    # PLOT HEATMAPS
    # Define the figure
    fig, axes = plt.subplots(1, 2, figsize=(16, 5))
    axes = axes.flatten()

    # create list of intensities for labelling of y-axis
    intensinvert = []

    for i in enumerate(intens):
        intensinvert.append(i)

    yintens = intensinvert.reverse()

    # create subplots
    fig, ax, qm = mca.plot_coefficient_heatmap(
        intensDF,
        title="Concentration Control",
        rows=yintens,
        annotate=False,
        ax=axes[0],
        norm=TwoSlopeNorm(0),
    )
    qm.colorbar.ax.tick_params(labelsize=14)

    fig, ax, qm = mca.plot_coefficient_heatmap(
        intensFDF,
        title="Flux Control",
        rows=yintens,
        annotate=False,
        ax=axes[1],
        norm=TwoSlopeNorm(0),
    )
    qm.colorbar.ax.tick_params(labelsize=14)

    # Warp up the plot
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.7)

    return fig, axes


def coefficients_heatmap(model, parameter, compound, reaction, return_plot=True):
    # Calculate the response coefficients for pfd = 440
    intens = np.linspace(20, 200, 10)

    ccc440, fcc440 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 440
    )

    # Calculate the response coefficients for pfd = 480
    ccc480, fcc480 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 480
    )

    # Calculate the response coefficients for pfd = 550
    ccc550, fcc550 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 550
    )

    # Calculate the response coefficients for pfd = 590
    ccc590, fcc590 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 590
    )

    # Calculate the response coefficients for pfd = 624
    ccc624, fcc624 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 624
    )

    # Calculate the response coefficients for pfd = 674
    ccc674, fcc674 = _calculate_coefficients(
        model, parameter, compound, reaction, intens, 674
    )

    # make concentration coefficients into pandas series
    d = {
        "440": pd.Series(ccc440, index=intens),
        "480": pd.Series(ccc480, index=intens),
        "550": pd.Series(ccc550, index=intens),
        "590": pd.Series(ccc590, index=intens),
        "624": pd.Series(ccc624, index=intens),
        "674": pd.Series(ccc674, index=intens),
    }

    # create DataFrame
    dfintens = pd.DataFrame(d)

    # convert DataFrame to have wavelength as rows and intensities as columns
    DFintens = dfintens.pivot_table(columns=intens)

    # invert order of columns to have lowest intensity at the bottom of the heatmap
    intensDF = DFintens.iloc[:, ::-1]

    # make flux coefficients into pandas series
    fd = {
        "440": pd.Series(fcc440, index=intens),
        "480": pd.Series(fcc480, index=intens),
        "550": pd.Series(fcc550, index=intens),
        "590": pd.Series(fcc590, index=intens),
        "624": pd.Series(fcc624, index=intens),
        "674": pd.Series(fcc674, index=intens),
    }

    # create DataFrame
    fdfintens = pd.DataFrame(fd)

    # convert DataFrame to have wavelength as rows and intensities as columns
    FDFintens = fdfintens.pivot_table(columns=intens)

    # invert order of columns to have lowest intensity at the bottom of the heatmap
    intensFDF = FDFintens.iloc[:, ::-1]

    if return_plot:
        return plot_coefficients_heatmap(intensDF, intensFDF, intens)
    else:
        return intensDF, intensFDF, intens
