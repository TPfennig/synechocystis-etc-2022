#!/usr/bin/env python

import numpy as np
import parameters

# CHANGE LOG
# 017 13.05.2023 | created update

# Define the updated description
def _update_compound_algebraic_modules(m, old, new):
    for nam in m.algebraic_modules.keys():
        args = m.get_algebraic_module_args(nam)
        if old in args:
            args[args == old] = new
            m.update_algebraic_module_from_args(nam, args=args)
    return m


def _update_compound_rates(m, old, new):
    for nam in m.rates.keys():
        args = m.get_rate_args(nam)
        if old in args:
            args[args == old] = new
            m.update_reaction_from_args(nam, args=args)
    return m


def update_GAP(m, y0, init_param=None, verbose=True):
    if verbose:
        print("updating sugar to GAP")

    # Remove the old description
    m = m.copy()
    m.remove_compound("3PGA")
    m.add_compound("GAP")

    # Get the rates that need to be updated
    update_rates = list(m.get_stoichiometries_by_compounds()["3PGA"].keys())

    for nam in update_rates:
        # Update the stoichiometries with GAP, and add 1 NADPH as counter metabolite
        cur_stoich = m.get_rate_stoichiometry(nam)
        cur_3PGA = cur_stoich["3PGA"]
        cur_NADPH = cur_stoich.get("NADPH", None)

        new_stoich = {k: v for k, v in cur_stoich.items() if k not in ["3PGA", "NADPH"]}
        new_stoich.update({"GAP": cur_3PGA})
        if cur_NADPH is not None:
            new_stoich.update({"NADPH": cur_NADPH - cur_3PGA})
            cur_Ho = cur_stoich.get("Ho", None)
            new_stoich.update({"Ho": cur_Ho - (cur_3PGA / m.get_parameter("bHo"))})

        m.update_stoichiometry(nam, new_stoich)

    # Replace 3PGA in rates and algebraic modules
    m = _update_compound_algebraic_modules(m, "3PGA", "GAP")
    m = _update_compound_rates(m, "3PGA", "GAP")

    y0["GAP"] = y0["3PGA"]
    y0.pop("3PGA")
    return m, y0
