#!/usr/bin/env python

### Export ###

import re

import numpy as np
import pandas as pd
from tabulate import tabulate

from calculate_parameters_restruct import make_readable_value


# Select the parameters to show and define sub-headlines
params_select = {
    "Cell specific": [
        # Cell specific
        "V_cell",
        "n_chl",
        "bHi",
        "bHo",
        "f_V_lumen",
        "V_cyt",
        "V_lumen",
        "cChl",
        "chl_cyt",
        "chl_lumen",
        "cf_lumen",
        "cf_cytoplasm",
        "fCin",
        "HPR",
        "pigment_content",
        "PBS_free",
        "PBS_PS1",
        "PBS_PS2",
        "fluo_influence",
    ],
    "Constants": [
        # Constants
        "F",
        "R",
        "T",
        "NA",
        "M_chl",
        "M_CO2",
        "DeltaG0_ATP",
        # "Kw",
    ],
    "Compound concentrations": [
        # Compound concentrations
        "PSIItot",
        "PSItot",
        "Q_tot",
        "PC_tot",
        "Fd_tot",
        "NADP_tot",
        "NAD_tot",
        "AP_tot",
        "Pi_mol",
        "S",
    ],
    "Standard electrode potentials": [
        # Standard electrode potentials
        "E0_QA",
        "E0_PQ",
        "E0_PC",
        "E0_P700",
        "E0_FA",
        "E0_Fd",
        "E0_O2/H2O",
        "E0_NADP",
        "E0_succinate/fumarate",
    ],
    "Kinetic constants": [
        # Kinetic constants
        "kH0",
        "kHst",
        "kF",
        "k2",
        "k_F1",
        "kPQred",
        "kPCox",
        "kFdred",
        "k_Q",
        "k_NDH",
        "k_SDH",
        "k_ox1",
        "k_O2",
        "k_aa",
        "k_NQ",
        "k_FN_fwd",
        "k_FN_rev",
        "kRespiration",
        "kO2out",
        "kCCM",
        "k_pass",
        "kATPsynth",
        "kATPconsumption",
        "kNADHconsumption",
    ],
    "Regulation kinetic constants": [
        # Regulation kinetic constants
        "kQuench",
        "kUnquench",
        "KMUnquench",
        "nUnquench",
        # "kFlvactivation",
        "KHillFdred",
        "nHillFdred",
        "kCBBactivation",
        "KMFdred",
        "OCPmax",
        "kOCPactivation",
        "kOCPdeactivation",
    ],
    "CBB and PR parameters": [
        # CBB and PR parameters
        "vCBB_max",
        "vOxy_max",
        "KMATP",
        "KMNADPH",
        "KMCO2",
        "KICO2",
        "KMO2",
        "KIO2",
        # "KEH",
        # "KEOH",
        "KMPGA",
        "kPR",
    ],
}
params_select_all = np.concatenate([x for x in params_select.values()])

_columns = {
    "name": "Parameter",
    "value": "Value",
    "descr": "Description",
    "source": "Source value",
    "ref": "Source",
}


def make_parameter_string_df(p):
    # Make a new dictionary to store the readable values
    p_df = pd.Series(dtype=str)

    for key, value in p.items():
        try:
            if isinstance(value, (int, float)):
                prnt = str(make_readable_value(value))
            elif isinstance(value, list):
                prnt = "["
                for i, sub_val in enumerate(value):
                    prnt += str(make_readable_value(sub_val))

                    if i < len(value) - 1:
                        prnt += ", "
                prnt += "]"
            elif isinstance(value, (dict, pd.Series)):
                prnt = "{"
                for i, (sub_k, sub_val) in enumerate(value.items()):
                    prnt += f"{sub_k}: " + str(make_readable_value(sub_val))

                    if i < len(value) - 1:
                        prnt += ", "
                prnt += "}"
            else:
                raise ValueError(f"parameter {key} has unsupported type {type(value)}")

            # Save the new readable value
            p_df[key] = prnt
        except:
            raise RuntimeError(f"parameter {key} could not be made readable")

    return p_df


def _check_completeness(params, params_select, reverse_search=False):
    params_select_list = np.concatenate(
        [np.array(params) for params in params_select.values()]
    )
    params_list = np.array(list(params.keys()))

    if reverse_search:
        included = np.in1d(params_select_list, params_list)
    else:
        included = np.in1d(params_list, params_select_list)

    if np.all(included):
        return True, None
    else:
        return False, (
            params_select_list[np.invert(included)]
            if reverse_search
            else params_list[np.invert(included)]
        )


def _check_duplicated(params_select):
    params_select_list = np.concatenate(
        [np.array(params) for params in params_select.values()]
    )
    dupl = pd.Series(params_select_list).duplicated()
    if dupl.any():
        return True, set(params_select_list[dupl])
    else:
        return False, None


def get_refd(
    items,
    no_ref=[
        "guess",
        "estimated",
        "fit to supply CBB",
        "visually fitted",
    ],
    other=["derived", "set"],
):
    sources = np.array([item["ref"] for item in items.values()])
    nonrefd_bool = np.in1d(sources, no_ref)
    other_bool = np.in1d(sources, other)

    keys = np.array(list(items.keys()))

    return {
        "refd": keys[np.invert(np.logical_or(nonrefd_bool, other_bool))],
        "non-refd": keys[nonrefd_bool],
        "other": keys[other_bool],
    }


# Parameters are stored in params.p
# First convert the dict of dicts into a df
def export_parameters_to_latex(
    params,
    params_select=params_select,
    params_columns=_columns,
    dontcite=[
        "derived",
        "guess",
        "estimated",
        "set",
        "fit to supply CBB",
        "visually fitted",
    ],
    cite_command="\cite",
    do_checks=True,
):
    if "name" not in params_columns.keys():
        raise ValueError("'name' must be in params_columns")
    elif "value" not in params_columns.keys():
        raise ValueError("'value' must be in params_columns")
    else:
        params_columns_noname = {
            key: val for key, val in params_columns.items() if key != "name"
        }
        _params_columns, _params_headers = (
            params_columns_noname.keys(),
            params_columns_noname.values(),
        )
        _params_headers = [params_columns["name"]] + list(_params_headers)

    # check the params_select soundness
    if do_checks:
        compl = _check_completeness(params, params_select)
        dupl = _check_duplicated(params_select)

        if not compl[0]:
            raise ValueError(f"params_select is missing parameters: {compl[1]}")
        elif dupl[0]:
            raise ValueError(f"params_select contains duplicate parameters: {dupl[1]}")

    p_df = pd.DataFrame(params).T

    # Convert the value column into strings
    p_df["value"] = make_parameter_string_df(p_df["value"])
    p_df = p_df.astype(str)

    # Add citation command via placeholders
    citebool = np.invert(p_df["ref"].isin(dontcite))

    p_df["ref"][citebool] = "xXXX" + p_df["ref"][citebool] + "XXXx"

    # Join the value and unit columns
    p_df["value"] = p_df["value"] + " [" + p_df["unit"] + "]"
    p_df = p_df.drop("unit", axis=1)

    # Create the table
    # Create the string to hold the table
    p_ltx = ""

    # Iterate through the selected parameter groups
    for i, (key, value) in enumerate(params_select.items()):
        if i == 0:
            # The first part of the table is given headers
            temp = tabulate(
                p_df.loc[value, _params_columns],
                tablefmt="latex_booktabs",
                headers=_params_headers,
            )
        else:
            # The toprule of all other is deleted
            temp = tabulate(p_df.loc[value, _params_columns], tablefmt="latex_booktabs")
            temp = re.sub("\\\\begin{tabular}{\w*}\\n\\\\toprule", "\\\\midrule", temp)

        if i < len(params_select) - 1:
            # Delete the bottomrule
            temp = re.sub("\\\\bottomrule\\n\\\\end{tabular}$", "", temp)

        temp = re.sub(
            "\\\\midrule\\n",
            "\\\\midrule\\n"
            + "\\\\multicolumn{5}{c}{%s}\\\\\\\\\\n" % key
            + "\\\\midrule\\n",
            temp,
        )
        p_ltx += temp

    # # p_df["ref"] = "\citet{" + p_df["ref"] +  "}"
    p_ltx = p_ltx.replace("xXXX", f"{cite_command}{{")
    p_ltx = p_ltx.replace("XXXx", "}")

    return p_ltx


# Initial values are stored in params.c
# First convert the dict of dicts into a df
def export_concs_to_latex(
    concs,
    concs_columns=_columns,
    dontcite=[
        "derived",
        "guess",
        "estimated",
        "set",
        "fit to supply CBB",
        "visually fitted",
    ],
    cite_command="cite",
):
    if "name" not in concs_columns.keys():
        raise ValueError("'name' must be in concs_columns")
    elif "value" not in concs_columns.keys():
        raise ValueError("'value' must be in concs_columns")
    else:
        concs_columns_noname = {
            key: val for key, val in concs_columns.items() if key != "name"
        }
        _concs_columns, _params_headers = (
            concs_columns_noname.keys(),
            concs_columns_noname.values(),
        )
        _params_headers = [concs_columns["name"]] + list(_params_headers)

    n_df = pd.DataFrame(concs).T

    # Convert the value column into strings
    n_df["value"] = make_parameter_string_df(n_df["value"])
    n_df = n_df.astype(str)

    # Join the value and unit columns
    n_df["value"] = n_df["value"] + " [" + n_df["unit"] + "]"
    n_df = n_df.drop("unit", axis=1)

    # Add citation command via placeholders
    citebool = np.invert(n_df["ref"].isin(dontcite))

    n_df["ref"][citebool] = "xXXX" + n_df["ref"][citebool] + "XXXx"

    # Make latex table
    n_ltx = tabulate(
        n_df.loc[:, _concs_columns], tablefmt="latex_booktabs", headers=_params_headers
    )

    # Replace the placeholders for the citation command
    n_ltx = n_ltx.replace("xXXX", f"{cite_command}{{")
    n_ltx = n_ltx.replace("XXXx", "}")

    return n_ltx


def _check_if_used(used, used_updates):
    included = True if used["if"] is None else np.any(np.in1d(used["if"], used_updates))
    excluded = (
        False if used["not"] is None else np.any(np.in1d(used["not"], used_updates))
    )
    return included and not excluded


def get_model_parameters(p, used_updates):
    p_new = {k: v for k, v in p.items() if _check_if_used(v["used"], used_updates)}
    return p_new


if __name__ == "__main__":
    from calculate_parameters_restruct import p, pu, c, cu

    # Set the model updates used
    used_updates = [
        "add_OCP",
        "update_statetransitions_hill",
        "update_Flv_hill",
        "update_CCM",
        "update_CBBactivation_MM",
    ]

    # Collect all parameters and concentrations
    p.update(pu)
    c.update(cu)

    # Move parameters that are stored in c to p
    to_move = [k for k in c.keys() if k in params_select_all]
    # Also check for duplicates in p and c, exclude from c

    p_new = p.copy()
    p_new.update({k: v for k, v in c.items() if k in to_move})

    c_new = {k: v for k, v in c.items() if k not in (to_move + list(p.keys()))}

    # Exclude get the parameters that fit to the used updates
    p_new = get_model_parameters(p_new, used_updates)
    c_new = get_model_parameters(c_new, used_updates)

    # Check if all selected parameters are still in the parameter list
    good_select = _check_completeness(p_new, params_select, reverse_search=True)
    if not good_select[0]:
        print(
            f"params_select not up to date, its keys {good_select[1]} are not in the parameters"
        )

    p_ltx = export_parameters_to_latex(p_new)
    c_ltx = export_concs_to_latex(c_new)

    with open("out/table_parameters.tex", "w") as f:
        f.write(p_ltx)
    with open("out/table_concentrations.tex", "w") as f:
        f.write(c_ltx)
